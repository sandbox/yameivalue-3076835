<?php

namespace Drupal\ignore_page_cache\IgnorePageCache;

use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Request;

class IgnorePageCache extends PageCache
{
    /**
     * Strips parameters from Request
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   A request object.
     *
     * @return string
     *  Stripped request uri
     */
    protected function stripQueryTags(Request $request){
        foreach( $this->paramsToIgnore() as $param ) {
            $request->query->remove($param);
        }
        $params = $request->query->all();
        return strtok($request->getRequestUri(),'?') . (($params) ? '?' : '') . http_build_query($params);
    }

    /**
     * @return array
     */
    protected function paramsToIgnore(){
        $config = \Drupal::config('ignore_page_cache.settings');

        return explode(',', $config->get('params_to_exclude')['exclude_tags']);
    }

    /**
     * Gets the page cache ID for this request.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   A request object.
     *
     * @return string
     *   The cache ID for this request.
     */
    protected function getCacheId(Request $request) {

        if (!isset($this->cid)) {
            $cid_parts = [
                $request->getSchemeAndHttpHost() . $this->stripQueryTags($request),
                $request->getRequestFormat(NULL),
            ];
            $this->cid = implode(':', $cid_parts);
        }

        return $this->cid;
    }
}