<?php

namespace Drupal\ignore_page_cache\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class SettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'ignore_page_cache';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'ignore_page_cache.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('ignore_page_cache.settings');

        $params = $config->get('params_to_exclude')['exclude_tags'];

        $form['exclude_tags'] = [
            '#type' => 'textfield',
            '#prefix' => '<h2>Fill in query parameters that should be ignored by Page Cache.</h2>',
            '#title' => $this->t('Query parameters to exclude'),
            '#suffix' => 'Add multiple tags sepparted with ,',
            '#default_value' => $params,
        ];

        $form['add_query_parameter'] = array(
            '#type' => 'button',
            '#value' => t('Add Query Parameter'),
            '#href' => '',
            '#ajax' => array(
                'callback' => 'custom_registration_ajax_add_param',
                'wrapper' => 'param',
            ),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();

        $this->config('ignore_page_cache.settings')
            ->set('params_to_exclude', $values)
            ->save();

        parent::submitForm($form, $form_state);
    }

}