<?php

namespace Drupal\ignore_page_cache;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

class IgnorePageCacheServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {
    public function alter(ContainerBuilder $container){
        $definition = $container->getDefinition('http_middleware.page_cache');
        $definition->setClass('Drupal\ignore_page_cache\IgnorePageCache\IgnorePageCache');
    }
}